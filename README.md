# Instructions for Running the System
The instructions of running the software can be found at: https://github.com/m-hasan-n/hlp, 
which is the code repository for the prior work on HLP [1]. The software is written in Matlab, 
so having Matlab is a preliminary step to running the system. 

Moreover, to train and test the models implemented in this project, the following steps should be taken:
1.	Obtain the source for hlp, from the repository given above.
2.	Open the hlp code in Matlab and run “install_hlp” in the command window.
3.	Segment the demonstrations by running “load_segment_demonstrations”.
4.	Make sure that all the subject directories are in the “segmented-demonstrations” directory.
5.	Obtain the source from the code repository of this project.
6.	Select a desired model by following the described hierarchy of directories of the deliverable.
7.	Copy the “dev” and “segmented-demonstrations” directories from the selected model’s directory.
8.	Paste the copied directories in the outer directory of the hlp system, select to replace the files in “dev” and “segmented-demonstrations”.
9.	Run “hlp_experiment_protocols(0, 1, '80_20', 0)” to train the model.
10.	Finally, when finished training run “hlp_experiment_protocols(0, 0, '80_20', 0)” to test the trained model.
11.	The testing results for each fold should be located under the “trained-models” directory.

References:
1. Hasan, M., Warburton, M., Agboh, W. C. et al. 2020. “Introducing a Human-like Planner for Reaching in Cluttered Environments”. To be published in ICRA 2020.