
for ii = 1:5
    SVM_test_dir = fullfile(curr_dir, 'trained-models','SVM_Gaussian','testing',...
    ['80_20_testing_results_1_' num2str(ii)]);

    DT_test_dir = fullfile(curr_dir, 'trained-models','Decision_Tree','testing',...
    ['80_20_testing_results_1_' num2str(ii)]);

    DTM_test_dir = fullfile(curr_dir, 'trained-models','DT_mixed_SVM','testing',...
    ['80_20_testing_results_1_' num2str(ii)]);

    load(SVM_test_dir);
    fprintf("SVM:\n");
    fprintf("gap_acc: %f\nmean_planning_time: %f\nobj_acc: %f\n"+...
        "trial_decision_acc: [%f, %f, %f, %f]\nmean_hlp_similarity: %f\n",...
        gap_acc,mean_planning_time, obj_acc, trial_decision_acc, mean_hlp_metric);
    
    load(DT_test_dir);
    fprintf("DT:\n");
    fprintf("gap_acc: %f\nmean_planning_time: %f\nobj_acc: %f\n"+...
        "trial_decision_acc: [%f, %f, %f, %f]\nmean_hlp_similarity: %f\n",...
        gap_acc,mean_planning_time, obj_acc, trial_decision_acc, mean_hlp_metric);
    
    load(DTM_test_dir);
    fprintf("DTM:\n");
    fprintf("gap_acc: %f\nmean_planning_time: %f\nobj_acc: %f\n"+...
        "trial_decision_acc: [%f, %f, %f, %f]\nmean_hlp_similarity: %f\n\n",...
        gap_acc,mean_planning_time, obj_acc, trial_decision_acc, mean_hlp_metric);
    
    
end