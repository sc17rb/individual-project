SVM_test_dir = fullfile(curr_dir, 'trained-models','SVM_Gaussian', ...
    'original','testing');
% comparison_test_dir = fullfile(curr_dir, 'trained-models','SVM_Gaussian','testing');
current_test_dir = fullfile(curr_dir, 'trained-models');

svm_total_gap_acc = 0;
svm_total_obj_acc = 0;
svm_total_hlp = 0;
svm_total_time = 0;
svm_total_trial_acc = [0 0 0 0];

for ii = 1:5
    load([SVM_test_dir '\80_20_testing_results_1_' num2str(ii)]);
    
    svm_total_gap_acc = svm_total_gap_acc + gap_acc;
    svm_total_obj_acc = svm_total_obj_acc + obj_acc;
    svm_total_hlp = svm_total_hlp + mean_hlp_metric;
    svm_total_time = svm_total_time + mean_planning_time;
    svm_total_trial_acc = svm_total_trial_acc + trial_decision_acc;
    
end

svm_gap_acc = svm_total_gap_acc / 5;
svm_obj_acc = svm_total_obj_acc / 5;
svm_hlp = svm_total_hlp / 5;
svm_time = svm_total_time / 5;
svm_trial_acc = svm_total_trial_acc ./ 5;

fprintf("SVM:\n");
fprintf("gap_acc: %f\nmean_planning_time: %f\nobj_acc: %f\n"+...
        "trial_decision_acc: [%f, %f, %f, %f]\nmean_hlp_similarity: %f\n",...
        svm_gap_acc, svm_time, svm_obj_acc, svm_trial_acc, svm_hlp);
    
total_gap_acc = 0;
total_obj_acc = 0;
total_hlp = 0;
total_time = 0;
total_trial_acc = [0 0 0 0];
    
for ii = 1:5
    load([current_test_dir '\80_20_testing_results_1_' num2str(ii)]);
    
    total_gap_acc = total_gap_acc + gap_acc;
    total_obj_acc = total_obj_acc + obj_acc;
    total_hlp = total_hlp + mean_hlp_metric;
    total_time = total_time + mean_planning_time;
    total_trial_acc = total_trial_acc + trial_decision_acc;

end

gap_acc = total_gap_acc / 5;
obj_acc = total_obj_acc / 5;
hlp = total_hlp / 5;
time = total_time / 5;
trial_acc = total_trial_acc ./ 5;

fprintf("\nCurrent:\n");
fprintf("gap_acc: %f\nmean_planning_time: %f\nobj_acc: %f\n"+...
        "trial_decision_acc: [%f, %f, %f, %f]\nmean_hlp_similarity: %f\n",...
        gap_acc, time, obj_acc, trial_acc, hlp);
